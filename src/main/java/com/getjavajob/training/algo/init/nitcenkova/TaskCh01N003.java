package com.getjavajob.training.algo.init.nitcenkova;
/*
TODO:
    Составить программу вывода на экран числа, вводимого с клавиатуры.
    Выводимому числу должно предшествовать сообщение "Вы ввели число".
 */
import java.util.Scanner;

public class TaskCh01N003 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println("Вы ввели число" + " " + input);
    }
}