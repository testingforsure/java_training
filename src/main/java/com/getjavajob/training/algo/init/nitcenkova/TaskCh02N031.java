package com.getjavajob.training.algo.init.nitcenkova;

import java.util.Scanner;

/* TODO: В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному
    при этом двузначному числу справа приписали вторую цифру числа x, то получилось число n.
    По заданному n найти число x (значение n вводится с клавиатуры, 100 ≤ n ≤ 999).
 */
public class TaskCh02N031 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        System.out.println(getOriginalNumber(input));
    }

    public static int getOriginalNumber(Integer input) {

        int[] originalNumberArray = new int[3];
        originalNumberArray[0] = input / 100;
        originalNumberArray[2] = (input / 10) % 10;
        originalNumberArray[1] = input % 10;

        int originalNumber = 0;
        for (int i = 0; i < originalNumberArray.length; i++) {
            originalNumber = originalNumber * 10 + originalNumberArray[i];
        }
        return originalNumber;
    }
}