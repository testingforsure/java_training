package com.getjavajob.training.algo.init.nitcenkova;

import java.util.Scanner;

/* TODO Даны два целых числа a и b. Если a делится на b или b делится на a, то вывес-ти 1, иначе — любое другое число.
    Условные операторы и операторы цикла не использовать.
 */
public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(isSame(a,b));
    }
    public static int isSame(int a, int b) {
        return (a % b) * (b % a) + 1;
    }
}