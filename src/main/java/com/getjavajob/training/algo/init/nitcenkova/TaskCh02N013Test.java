package com.getjavajob.training.algo.init.nitcenkova;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N013Test {

    public static void main(String[] args) {
        testIsNumberReversed();
    }
    public static void testIsNumberReversed() {
        assertEquals("TaskCh02N013Test.testIsNumberReversed", TaskCh02N013.reverseNumber(246), 642);
    }
}