package com.getjavajob.training.algo.init.nitcenkova;

import java.util.Scanner;

/*
TODO: Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие момент времени: "h часов, m минут, s секунд".
 Определить угол (в градусах) между положением часовой стрелки в начале суток и в указанный момент времени.
*/
public class TaskCh02N039 {

    public static void main(String[] args) {

        Scanner scaner = new Scanner(System.in);
        System.out.println("Type hours");
        int hours = scaner.nextInt() % 12;
        System.out.println("Type minutes");
        int minutes = scaner.nextInt() % 59;
        System.out.println("Type seconds");
        int seconds = scaner.nextInt() % 59;

        System.out.println(getAngle(hours, minutes, seconds));

    }

    public static float getAngle(int hours, int minutes, int seconds) {

        float hourHand = (float) 30 * hours;
        float minuteHand = (float) minutes / 60 * 30;
        float secondHand = (float) seconds / 3600 * 30;

        return hourHand + minuteHand + secondHand;
    }
}