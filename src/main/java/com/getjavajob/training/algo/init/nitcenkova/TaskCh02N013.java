package com.getjavajob.training.algo.init.nitcenkova;

/*
TODO: Дано трехзначное число 100<x<200. Найти число, полученное при прочтении его цифр справа налево.
 */
public class TaskCh02N013 {

    public static void main(String[] args) {

        int number = getNumber();
        System.out.println(number);
        System.out.println(reverseNumber(number));
    }

    public static Integer getNumber() {
        return (int) ((Math.random() * (200 - 100)) + 100);
    }

    public static int reverseNumber(int number) {
        int[] reversedNumber = new int[3];
        reversedNumber[0] = number % 10;
        reversedNumber[1] = (number / 10) % 10;
        reversedNumber[2] = number / 100;
        int finalNumber = 0;
        for (int i = 0; i < reversedNumber.length; i++) {
            finalNumber = finalNumber * 10 + reversedNumber[i];
        }
        return finalNumber;
    }
}