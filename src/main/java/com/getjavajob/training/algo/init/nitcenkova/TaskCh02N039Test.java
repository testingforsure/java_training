package com.getjavajob.training.algo.init.nitcenkova;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        testIsAngleCorrect();
    }
    public static void testIsAngleCorrect(){
        assertEquals("TaskCh02N039Test.testIsAngleCorrect", TaskCh02N039.getAngle(5,30,0), 165);
    }
}